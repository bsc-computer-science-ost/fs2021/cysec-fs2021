\section{Identity and Access Management (IAM)}
An access control is any hardware, software, or administrative policy or procedure that controls access to resources. The goal is to provide access to authorized subjects and prevent unauthorized access attempts.

\begin{description}
	\item[Subject] \hfill \\
	A subject is an active entity that accesses a passive object to receive information from, or data about, an object. Subjects can be users, programs, processes, services, computers, or anything else that can access a resource. 
	\item[Object] \hfill \\
	An object is a passive entity that provides information to active subjects. Some examples of objects include files, databases, computers, programs, processes, services, printers, and storage media.
\end{description}

\subsection{Controlling access to assets}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{accesscontroltypes}
  \caption{Access Control Types}
\end{figure}

\begin{description}
	\item[Preventive Access Control] \hfill \\
	A preventive control attempts to thwart or stop unwanted or unauthorized activity from occurring. (fences, locks, biometrics, mantraps, lighting)
	\item[Detective Access Control] \hfill \\
	A detective control attempts to discover or detect unwanted or unauthorized activity. Detective controls operate after the fact and can discover the activity only after it has occurred. (security guards, motion detectors, recording and reviewing of events captured by security cameras or CCTV)
	\item[Corrective Access Control] \hfill \\
	A corrective control modifies the environment to return systems to normal after an unwanted or unauthorized activity has occurred. Corrective controls attempt to correct any problems that occurred because of a security incident. (Rebooting a system, antivirus solutions that can remove or quarantine a virus)
\end{description}

There are additional access control types to further differentiate between the different controls.

\begin{description}
	\item[Deterrent] \hfill \\
	A deterrent control is deployed to discourage Deterrent Compensating Directive violation of security policies. Deterrent and preventive controls are similar, but deterrent controls often depend on individuals deciding not to take an unwanted action. In contrast, a preventive control actually blocks the action. (Policies, security-awareness training)
	\item[Compensating] \hfill \\
	A compensation control is deployed to provide various options to other existing controls to aid in enforcement and support of security policies. They can be any controls used in addition to, or in place of, another control.
	\item[Directive] \hfill \\
	A directive control is deployed to direct, confine, or control the actions of subjects to force or encourage compliance with security policies. (Security policy requirements or criteria, posted notifications, escape route exit signs, monitoring, supervision, and procedures.)
	\item[Recovery] \hfill \\
	Recovery controls are an extension of corrective controls but have more advanced or complex abilities. (Backups and restores, fault-tolerant drive systems, system imaging, server clustering, antivirus software)
\end{description}

\subsection{Implementation categorization}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{accategorization}
  \caption{Implemenation Categories}
\end{figure}

\begin{description}
	\item[Physical controls] \hfill \\
	are items you can physically touch. They include physical mechanisms deployed to prevent, monitor, or detect direct contact with systems or areas within a facility.
	\item[Technical or logical controls] \hfill \\
	involve the hardware or software mechanisms used to manage access and to provide protection for resources and systems. As the name implies, it uses technology.
	\item[Administrative controls] \hfill \\
	are the policies and procedures defined by an organization’s security policy and other regulations or requirements. They are sometimes referred to as management controls. These controls focus on personnel and business practices.
\end{description}

\subsection{The Steps of access control}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.35\textwidth]{stepsofac}
  \caption{Access Control Flow}
\end{figure}

\subsubsection{Identification}
Identification is the process of a subject claiming, or professing, an identity. Providing an identity might entail typing a username, swiping a smartcard, waving a token device, speaking a phrase, or positioning your face, hand, or finger in front of a camera or in proximity to a scanning device.  Simply claiming an identity does not imply access or authority. The identity must be proven (authentication) or verified (ensuring nonrepudiation) before access to controlled resources is allowed (verifying authorization).

\subsubsection{Authenthication}
Authentication requires the subject to provide additional information that corresponds to the identity they are claiming. The most common form of authentication is using a password. They are the weakest form of authentication. 

\begin{description}
	\item[Password Storage] \hfill \\
	Passwords are rarely stored in plaintext. A system will create a hash of a password using a hashing algorithm. 
	\item[Creating Strong Passwords] \hfill \\
	Passwords are most effective when users create strong passwords. A strong password is sufficiently long and uses multiple character types such as uppercase letters, lowercase letters, numbers, and special characters. 
	\item[Password Phrases ] \hfill \\
	A password mechanism that is more effective than a basic password is a passphrase. A passphrase is a string of characters similar to a password but that has unique meaning to the user. \\ “IPassedTheCySecExam” or “1P@ssedTheCySecEx@m”
	\item[Cognitive Passwords] \hfill \\
	A cognitive password is a series of challenge questions about facts or predefined responses that only the subject should know. 
	\item[Smartcards] \hfill \\
	A smartcard is a credit card–sized ID or badge and has an integrated circuit chip embedded in it. Smartcards contain information about the authorized user that is used for identification and/or authentication purposes.  Most current smartcards include a microprocessor and one or more certificates.
\end{description}

\begin{description}
	\item[Tokens] \hfill \\
	A token device, or hardware token, is a password-generating device that users can carry with them. An authentication server stores the details of the token, so at any moment, the server knows what number is displayed on the user’s token.
	\item[Synchronous Dynamic Password Tokens] \hfill \\
	Hardware tokens that create synchronous dynamic passwords are time-based and synchronized with an authentication server. They generate a new password periodically, such as every 60 seconds. This does require the token and the server to have accurate time.
	\item[Asynchronous Dynamic Password Tokens] \hfill \\
	The hardware token generates passwords based on an algorithm and an incrementing counter. When using an incrementing counter, it creates a dynamic onetime password that stays the same until used for authentication.
\end{description}

\begin{description}
	\item[Onetime Password Generators] \hfill \\
	Onetime passwords are dynamic passwords that change every time they are used. Onetime password generators are token devices that create passwords.
	\item[Time-based One-Time Password] \hfill \\
	Uses a timestamp and remains valid for a certain timeframe, such as 30 seconds. This is similar to the synchronous dynamic passwords used by tokens. 
	\item[HMAC-based One-Time Password] \hfill \\
	Includes a hash function to create onetime passwords. It creates HTOP values of six eight numbers. This is similar to the asynchronous dynamic passwords created by tokens. The HOTP value remains valid until used.
\end{description}

\paragraph{Authentication Factors}
There are three types of authentication factors. Multifactor authentication is any authentication using two or more factors and is therefore more secure in comparison to single factor authenthication. When two authentication methods of the same factor are used together, the strength of the authentication is no greater than it would be if just one method were used.

\begin{description}
	\item[Type 1 - Weak] \hfill \\
	A Type 1 authentication factor is something you know.
	\item[Type 2 - Normal] \hfill \\
	A Type 2 authentication factor is something you have. Physical devices that a user possesses can help them provide authentication.
	\item[Type 3 - Strong] \hfill \\
	A Type 3 authentication factor is something you are or something you do. It is a physical characteristic of a person identified with different types of biometrics.
\end{description}

\begin{figure}[h!]
 	\center
 	\includegraphics[width=0.75\textwidth]{afactors}
 	\caption{Authentication Factors}
\end{figure}



\subsubsection{Authorization}
Once a subject is authenticated, access must be authorized. The process of authorization ensures that the requested activity or access to an object is possible given the rights and privileges assigned to the authenticated identity. Identification and authentication are all-or-nothing aspects of access control. 

\begin{description}
	\item[Discretionary Access Control] \hfill \\
	Every object has an owner and the owner can grant or deny access to any other subjects. The New Technology File System (NTFS), used on Microsoft Windows operating systems, uses the DAC model.
	\item[Role-based Access Control] \hfill \\
	User accounts are placed in roles and administrators assign privileges to the roles. These roles are typically identified by job functions. If a user account is in a role, the user has all the privileges assigned to the role.
	\item[Rule-based access control] \hfill \\
	Global rules that are applied to all subjects. 
	\item[Attribute-based access control] \hfill \\
	Use of rules that can include multiple attributes. More flexible than a rule-based access control model that applies the rules to all subjects equally.
	\item[Manndatory-based access control] \hfill \\
	Use of labels applied to both subjects and objects. For Example, if a suer has a label of top secret, the user can be granted access to a top-secret document.
\end{description}

\paragraph{Understandig Authorization Mechanism}
\begin{description}
	\item[Implicity Deny] \hfill \\
	Basic principle of access control is implicit deny. The implicit deny principle ensures that access to an object is denied unless access has been explicitly granted to a subject.
	\item[Constrained Interface] \hfill \\
	Applications use constrained interfaces or restricted interfaces to restrict what users can do or see based on their privileges.
	\item[Access Control Matrix] \hfill \\
	An access control matrix is a table that includes subjects, objects, and assigned privileges. When a subject attempts an action, the system checks the access control matrix to determine if the subject has the appropriate privileges to perform the action. 
	\item[Capability Tables] \hfill \\
	Capability tables are another way to identify privileges assigned to subjects. Capability tables are subject focused and identify the objects than subjects can access.
	\item[Content-Dependent Control] \hfill \\
	Restrict access to data based on the content within an object. A database view is a content-dependent control. A view retrieves specific columns from one or products to a shopping cart and begin the checkout process. 
	\item[Context-Dependent Control] \hfill \\
	Context-dependent access controls require specific activity before granting users access. An example could be the checkout process of an online shop
\end{description}

\subsubsection{Auditing}
It is the programmatic means by which a subject’s actions are tracked and recorded. The purpose is to hold the subjects accountable for their actions while authenticated on a system. 

\subsubsection{Accounting}
Effective accountability relies on the capability to prove a subject’s identity and track their activities. Accountability is established by linking a human to the activities of an online identity through the security services and mechanisms of auditing, authorization, authentication, and identification. 

\subsection{Commont access control attacks}
\begin{description}
	\item[Access Aggregation Attacks (passive attack)] \hfill \\
	Access aggregation refers to collecting multiple pieces of nonsensitive information and aggregating them to learn sensitive information. Reconnaissance attacks are access aggregation attacks that combine multiple tools to identify multiple elements of a system, such as Internet Protocol (IP) addresses, open ports, running services, operating systems.
	\item[Password Attacks (Brute-force attack)] \hfill \\
	Online: Attacks against online accounts. \\
	Offline: to steal an account database and then crack the passwords.
	\item[Dictionary Attacks (Brute-force attack)] \hfill \\
	A dictionary attack is an attempt to discover passwords by using every possible password in a predefined database or list of common or expected passwords also called a password-cracking dictionaries. Dictionary attack databases also include character combinations commonly used as passwords, but not found in dictionaries. 
\end{description}

\begin{description}
	\item[Birthday Attack (Brute-force attack)] \hfill \\
	A birthday attack focuses on finding collisions. Its name comes from a statistical phenomenon known as the birthday paradox. The birthday paradox states that if there are 23 people in a room, there is a 50 percent chance that any two of them will have the same birthday. (This is not the same year, but instead the same month and day, such as March 30). With February 29 in a leap year, there are only 366 possible days in a year. • With 367 people in a room, you have a 100 percent chance of getting at least two people with the same birthdays. Reduce this to only 23 people in the room, and you still have a 50 percent chance that any two have the same birthday.\\
	You can reduce the success of birthday attacks by using hashing algorithms with enough bits to make collisions computationally infeasible, and by using salts. MD5 is not collision free. SHA-3 can use as many as 512 bits and is considered safe again birthday attacks and collisions - at least up to today.
	\item[Rainbow Table Attacks] \hfill \\
	A rainbow table reduces this time by using large databases of precomputed hashes. A password cracker can then compare every hash in the rainbow table against the hash in a stolen password database file.
	\item[Sniffer Attacks] \hfill \\
	A sniffer attack (also called eavesdropping attack) occurs when an attacker uses a sniffer to capture information transmitted over a network.
	\item[Spoofing Attacks] \hfill \\
	Spoofing (also known as masquerading) is pretending to be something, or someone, else. Types of spoofing used in access control attacks include email spoofing and phone number spoofing and IP spoofing.
	\item[Social Engineering Attacks] \hfill \\
	Social engineering occurs when an attacker attempts to gain the trust of someone. Educating employees on common social engineering tactics reduces the effectiveness of these types of attacks.
	\item[Shoulder surfing] \hfill \\
	Sometimes a social engineer just tries to look over the shoulder of an individual to read information on the computer screen or watch the keyboard as a user types. 
	\item[Phising] \hfill \\
	Phishing is a form of social engineering that attempts to trick users into giving up sensitive information, opening an attachment, or clicking a link. 
	\item[Spear Phising] \hfill \\
	Spear phishing is a form of phishing targeted to a specific group of users, such as employees within a specific organization.
	\item[Whaling] \hfill \\
	Whaling is a variant of phishing that targets senior or high-level executives such as chief executive officers (CEOs) and presidents within a company.
	\item[Vishing] \hfill \\
	While attackers primarily launch phishing attacks via email, they have also used other means to trick users, such as instant messaging (IM) and VoIP.
\end{description}

\subsection{Protection Mechanisms}

\begin{description}
	\item[Layering (defence in depth)] \hfill \\
	It is the use of multiple controls in a series. No one control can protect against all possible threats.
	\item[Abstraction] \hfill \\
	The concept of abstraction is used when classifying objects or assigning roles to subjects. Abstraction is used to define what types of data an object can contain, what types of functions can be performed on or by that object, and what capabilities that object has. Abstraction simplifies security by enabling you to assign security controls to a group of objects collected by type or function.
	\item[Data Hiding] \hfill \\
	Preventing data from being discovered or accessed by a subject by positioning the data in a logical storage compartment that is not accessible or seen by the subject. 
	\item[Security through obscurity] \hfill \\
	It is the idea of not informing a subject about an object being present and thus hoping that the subject will not discover the object. Security through obscurity does not actually implement any form of protection. It is instead an attempt to hope something important is not discovered by keeping knowledge of it a secret.
	\item[Encryption] \hfill \\
	Encryption is the art and science of hiding the meaning or intent of a communication from unintended recipients. Weak or poor encryption can be considered as nothing more than security through obscurity.
\end{description}
